/*
 * Main.java
 *
 * Created on December 11, 2007, 5:03 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package subscriber;

/**
 *
 * @author yuan
 */
public class Main
{
    private static String Subject;
    private static String Key;
    private static Subscriber FSubscriber;
    
    /** Creates a new instance of Main */
    public Main()
    {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {                
        // TODO code application logic here
        if(args.length < 2)
        {
            System.out.println("Please check the arguments.");
            System.exit(1);
        }
        else
        {
            Subject = args[0];
            Key = args[1];
            
            //System.out.printf("Subject : [%s] Key:[%s]\n", Subject, Key);
            System.out.println("Subject : ["+Subject+"] Key:["+Key+"]");
        }
        FSubscriber  = new Subscriber( "Subscriber", "127.0.0.1", 12345, Subject, Key );
        FSubscriber.Run();
    }
    
}
