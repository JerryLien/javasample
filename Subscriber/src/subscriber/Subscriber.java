/*
 * Subscriber.java
 *
 * Created on December 11, 2007, 5:04 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package subscriber;

import com.mdbs.messageDeliver.MAppListener;
import com.mdbs.messageDeliver.MApp;
import com.mdbs.messageDeliver.MTree;
import com.mdbs.messageDeliver.MessageListener;
import java.io.IOException;

/**
 *
 * @author yuan
 */
public class Subscriber extends MessageListener implements MAppListener {

    String FAppName;
    String FIPAddress;
    String FSubject;
    String FKey;
    int FPort;
    MApp FMApp;

    /** Creates a new instance of Subscriber */
    public Subscriber(String AppName, String IPAddress, int Port, String Subject, String Key) {
        FAppName = AppName;
        FIPAddress = IPAddress;
        FSubject = Subject;
        FKey = Key;
        FPort = Port;
        FMApp = new MApp(FAppName, FIPAddress, FPort, this);
        this.Create(FMApp, Subject, Key);
    }

    public void Run() {
        try {
            FMApp.Connect(10);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void OnMessage(String Subject, String Key, MTree Data) {
        //System.out.printf("---------------%s/%s----------------\n", Subject, Key);
        System.out.println("---------------"+Subject+"/"+Key+"----------------");
        int NodeCount = Data.GetNodeCount();
        for (int i = 0; i < NodeCount; i++) {
            String ValueName = Data.GetName(i);

            switch (Data.GetType(i)) 
            {
                case MTree.DataType.dtInt:
                    //System.out.printf("[%s]%d\n", ValueName, Data.get(ValueName, 0));
                    System.out.println("["+ValueName+"]"+Data.get(ValueName, 0));
                    break;
                case MTree.DataType.dtDouble:
                    //System.out.printf("[%s]%f\n", ValueName, Data.get(ValueName, 0.0));
                    System.out.println("["+ValueName+"]"+Data.get(ValueName, 0.0));
                    break;
                case MTree.DataType.dtString:
                    //System.out.printf("[%s]%s\n", ValueName, Data.get(ValueName));
                    System.out.println("["+ValueName+"]"+Data.get(ValueName));
                    break;
                default:
                    //System.out.printf("[%s] Unknown Type\n", ValueName);
                    System.out.println("["+ValueName+"] Unknown Type");
                    break;
            }
        }
        System.out.println("---------------------------------------");
    }

    public void OnConnect(MApp Transport) {
        //System.out.printf("%s OnConnect\n", FAppName);
        System.out.println(FAppName+" OnConnect");
    }

    public void OnDisconnect(MApp Transport) {
        //System.out.printf("%s OnDisconnect\n", FAppName);
        System.out.println(FAppName+" OnDisconnect");
    }

    public void OnError(MApp Transport, String ErrorMessage) {
        //System.out.printf("%s error:[%s]\n", FAppName, ErrorMessage);
        System.out.println(FAppName+" error["+ErrorMessage+"]");
    }
}
