/*
 * Main.java
 *
 * Created on December 11, 2007, 11:08 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package publisher;
/**
 *
 * @author yuan
 */
public class Main
{
    static Publisher FPublisher;    
    static String Subject;
    static String Key;
    private static int Interval;
    
    /** Creates a new instance of Main */
    public Main()
    {        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {  
        // TODO code application logic here
        if(args.length < 3)
        {
            System.out.println("Please check the arguments.");
            System.exit(1);
        }
        else
        {
            Subject = args[0];
            Key = args[1];
            Interval = java.lang.Integer.parseInt(args[2]);
            //System.out.printf("Subject : [%s] Key:[%s], Interval = [%d]\n", Subject, Key, Interval);
            System.out.println("Subject : ["+Subject+"] Key:["+Key+"], Interval = ["+Interval+"]");
        }        
        FPublisher = new Publisher("Java Publisher", "127.0.0.1", 12345, Interval, Subject, Key);                
        FPublisher.Run();

    }
    
}
