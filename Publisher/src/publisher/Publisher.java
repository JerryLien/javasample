/*
 * Publisher.java
 *
 * Created on December 11, 2007, 11:19 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package publisher;

import com.mdbs.messageDeliver.MApp;
import com.mdbs.messageDeliver.MTree;
import java.io.IOException;
/**
 *
 * @author yuan
 */
public class Publisher implements com.mdbs.messageDeliver.MAppListener
{
    MApp FMApp;
    String FIPAddress;
    String FAppName;
    int FPort;
    int FInterval;
    MTree Data;
    boolean IsConnect;
    String FSubject;
    String FKey;

    private int Seq = 0;
    /** Creates a new instance of Publisher */
    public Publisher( String AppName, String IPAddress, int Port, int Interval, String Subject, String Key )
    {
        FAppName = AppName;
        FIPAddress = IPAddress;
        FPort = Port;        
        FInterval = Interval;
        FSubject = Subject;
        FKey = Key;
        FMApp = new MApp(FAppName, FIPAddress, FPort, this);
        Data = new MTree();
    }
    
    public boolean Connect(int Timeout)
    {
        try
        {
            FMApp.Connect(Timeout);
            return true;
        } catch (IOException ex)
        {
            ex.printStackTrace();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    
    public void Disconnect()
    {
        FMApp.Disconnect();
    }

    public void Send(String Subject, String Key, MTree Data)
    {
        FMApp.Send(Subject, Key, Data, false);
    }
    
    public void OnConnect(MApp Transport)
    {
        IsConnect = true;
        //System.out.printf("%s OnConnect\n", FAppName);
        System.out.println(FAppName+" OnConnect");
    }

    public void OnDisconnect(MApp Transport)
    {
        IsConnect = false;
        //System.out.printf("%s OnDisconnect\n", FAppName);
        System.out.println(FAppName+" OnDisconnect");
    }

    public void OnError(MApp Transport, String ErrorMessage)
    {
        //System.out.printf("%s error:[%s]\n", FAppName, ErrorMessage);
        System.out.println(FAppName+" error:["+ErrorMessage+"]");
    }      
    
    public boolean IsConnect()
    {
        return FMApp.IsConnect();
    }
    
    public void Run()
    {
        if(Connect(10) == false)
            System.exit(6);
        try
        {
            java.lang.Thread.sleep(FInterval);
        } catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
        
        while(IsConnect == true)
        {            
            Data.Create();
            Data.append("Sequence", Seq );            
            FMApp.Send(FSubject, FKey, Data, false);
            Data.Destroy();
            Seq++;
            
            try
            {
                java.lang.Thread.sleep(FInterval);
            } catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
        return;
    }
        
}
